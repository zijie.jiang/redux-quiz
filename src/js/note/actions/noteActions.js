import {createNoteResource, deleteNoteResource, getNotesResource} from "../resources/noteResources";

export const GET_NOTES = 'GET_NOTES';
export const DELETE_NOTE_BY_ID = 'DELETE_NOTE_BY_ID';
export const CREATE_NOTE = 'CREATE_NOTE';

function getNotes() {
    return (dispatch) => getNotesResource().then(json => {
        dispatch({
            type: GET_NOTES,
            payload: json
        })
    })
}

function deleteNote(noteId, returnHomeCallback) {
    return (dispatch) => deleteNoteResource(noteId).then(() => {
        dispatch({
            type: DELETE_NOTE_BY_ID
        });
        returnHomeCallback();
    })
}

function createNote(title, description, returnHomeCallback) {
    return (dispatch) => createNoteResource(title, description).then(() => {
        dispatch({
            type: CREATE_NOTE
        });
        returnHomeCallback();
    })
}

export {
    getNotes,
    deleteNote,
    createNote
}
