import {GET_NOTES} from "../actions/noteActions";

const initState = {
    notes: []
};

export default function noteReducers(state = initState, action) {
    switch (action.type) {
        case GET_NOTES:
            return {
                ...state,
                notes: action.payload
            };
        default:
            return state;
    }
}
