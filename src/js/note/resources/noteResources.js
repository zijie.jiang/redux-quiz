const URL = 'http://127.0.0.1:8080/api/posts';

function getNotesResource() {
    return fetch(URL).then(response => response.json())
}

function deleteNoteResource(noteId) {
    return fetch(URL + `/${noteId}`, {
        method: 'DELETE'
    })
}

function createNoteResource(title, description) {
    return fetch(URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify({title, description})
    })
}

export {
    getNotesResource,
    deleteNoteResource,
    createNoteResource
}
