import React, {Component} from 'react';
import '../../../styles/NoteDetail.less';
import ButtonEvent from "../shared/components/ButtonEvent";
import ButtonLink from "../shared/components/ButtonLink";

class NoteDetail extends Component {

    constructor(props, context) {
        super(props, context);
        this.markdownToHtml = this.markdownToHtml.bind(this);
    }

    render() {
        const {note = {}, deleteNote} = this.props;
        return (
            <article className='note-detail'>
                <h2>{note.title}</h2>
                <div className='markdown' dangerouslySetInnerHTML={{__html: this.markdownToHtml()}}/>
                <ButtonEvent callback={e => deleteNote(e, note.id)} value='删除'/>
                <ButtonLink path='/' value='返回'/>
            </article>
        );
    }

    markdownToHtml() {
        const showdown = require('showdown'),
            showdownHighlight = require("showdown-highlight");
        const converter = new showdown.Converter({
            extensions: [showdownHighlight]
        });
        const {note = {}} = this.props;
        return converter.makeHtml(note.description);
    }
}

export default NoteDetail;
