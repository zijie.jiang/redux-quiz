import React, {Component} from 'react';
import {connect} from "react-redux";
import '../../../styles/NoteProducer.less'
import {createNote} from "../actions/noteActions";
import Header from "../shared/components/Header";
import ButtonEvent from "../shared/components/ButtonEvent";
import ButtonLink from "../shared/components/ButtonLink";
import returnHomeCallback from "../shared/components/Callback";

class NoteProducer extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            note: {
                title: '',
                description: ''
            }
        };
        this.disabled = this.disabled.bind(this);
        this.createNote = this.createNote.bind(this);
        this.bindNote = this.bindNote.bind(this);
    }

    render() {
        return (
            <article className="note-producer">
                <Header/>
                <h1>创建笔记</h1>
                <form>
                    <label>
                        <h4>标题</h4>
                        <input type='text' onChange={e => this.bindNote(e, 'title')}/>
                    </label>
                    <label>
                        <h4>正文</h4>
                        <textarea onChange={e => this.bindNote(e, 'description')}/>
                    </label>
                    <ButtonEvent callback={this.createNote} disabled={this.disabled} value='提交'/>
                    <ButtonLink path='/' value='取消'/>
                </form>
            </article>
        );
    }

    disabled() {
        const {title, description} = this.state.note;
        if (title.trim() === '' || description.trim() === '') {
            return 'disabled';
        }
    }

    bindNote(event, flag) {
        event.preventDefault();
        const {note} = this.state;
        const newNote = flag === 'title' ? {
            ...note,
            title: event.target.value
        } : {
            ...note,
            description: event.target.value
        };
        this.setState({
            note: newNote
        });
    }

    createNote(event) {
        event.preventDefault();
        const {title, description} = this.state.note;
        const {createNote} = this.props;
        createNote(title, description, returnHomeCallback(this.props));
    }
}

const mapDispatchToProps = (dispatch) => ({
    createNote: (title, description, returnHomeCallback) => dispatch(createNote(title, description, returnHomeCallback))
});

export default connect(undefined, mapDispatchToProps)(NoteProducer);
