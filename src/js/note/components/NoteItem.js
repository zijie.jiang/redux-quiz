import React, {Component} from 'react';
import {Link} from "react-router-dom";

class NoteItem extends Component {
    render() {
        const {note} = this.props;
        return (
            <Link to={`/notes/${note.id}`}>
                <span>{note.title}</span>
            </Link>
        );
    }
}

export default NoteItem;
