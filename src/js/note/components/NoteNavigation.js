import React from "react";
import {Link} from "react-router-dom";
import '../../../styles/NoteNavigation.less';

class NoteNavigation extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.selected = this.selected.bind(this);
    }

    render() {
        const {notes = []} = this.props;
        return (
            <ul>
                {
                    notes.map((item, index) =>
                        <li key={index}>
                            <Link to={`/notes/${item.id}`} style={this.selected(item.id)}>{item.title}</Link>
                        </li>)
                }
            </ul>
        );
    }

    selected(selectId) {
        const {match} = this.props;
        const {id} = match.params;
        if (selectId === parseInt(id)) {
            return {
                backgroundColor: '#DFE4EA',
                borderRadius: '5px'
            }
        }
    }
}

export default NoteNavigation;
