import React, {Component} from 'react';
import NoteItem from "./NoteItem";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {MdLibraryAdd} from "react-icons/md";
import {getNotes} from "../actions/noteActions";

class NoteList extends Component {
    render() {
        const {notes} = this.props;
        return (
            <div className="note-list">
                {
                    notes.map((item, index) => <NoteItem key={index} note={item}/>)
                }
                <Link to='/notes/create'><MdLibraryAdd/></Link>
            </div>
        );
    }

    componentDidMount() {
        const {getNotes} = this.props;
        getNotes();
    }
}

const mapStateToProps = ({noteReducers}) => ({
    notes: noteReducers.notes
});

const mapDispatchToProps = (dispatch) => ({
    getNotes: () => dispatch(getNotes())
});

export default connect(mapStateToProps, mapDispatchToProps)(NoteList);
