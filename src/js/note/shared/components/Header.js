import React from "react";
import {MdInsertInvitation} from "react-icons/md";
import '../../../../styles/Header.less';
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <header>
            <Link to='/'>
                <MdInsertInvitation/>
            </Link>
            <h1>NOTES</h1>
        </header>
    );
};

export default Header;
