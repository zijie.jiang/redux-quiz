export default function returnHomeCallback(props) {
    return () => props.history.push('/');
};
