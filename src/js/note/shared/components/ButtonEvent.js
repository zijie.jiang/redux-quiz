import React from "react";
import '../../../../styles/Button.less'

const ButtonEvent = ({callback, value, disabled = () => {}}) => {
    return (
        <button className="event" onClick={callback} disabled={disabled()}>{value}</button>
    );
};

export default ButtonEvent;
