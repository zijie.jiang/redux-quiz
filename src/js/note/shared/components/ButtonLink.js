import React from "react";
import {Link} from "react-router-dom";
import '../../../../styles/Button.less'

const ButtonLink = ({path, value}) => {
    return (
        <Link to={path}>
            <button>{value}</button>
        </Link>
    );
};

export default ButtonLink;
