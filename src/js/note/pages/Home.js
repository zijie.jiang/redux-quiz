import Header from "../shared/components/Header";
import React from "react";
import '../../../styles/Home.less';
import NoteList from "../components/NoteList";

const Home = () => {
    return (
        <div>
            <Header/>
            <NoteList/>
        </div>
    );
};

export default Home;
