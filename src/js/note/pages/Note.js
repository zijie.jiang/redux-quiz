import React from "react";
import NoteNavigation from "../components/NoteNavigation";
import NoteDetail from "../components/NoteDetail";
import {connect} from "react-redux";
import Header from "../shared/components/Header";
import {deleteNote} from "../actions/noteActions";
import returnHomeCallback from "../shared/components/Callback";

class Note extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.findNoteById = this.findNoteById.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
    }

    render() {
        const {notes, match} = this.props;
        return (
            <div>
                <Header/>
                <aside>
                    <NoteNavigation notes={notes} match={match}/>
                </aside>
                <NoteDetail note={this.findNoteById()} deleteNote={this.deleteNote}/>
            </div>
        )
    }

    findNoteById() {
        const {notes} = this.props;
        const {id} = this.props.match.params;
        return notes.filter(item => item.id === parseInt(id))[0];
    }

    deleteNote(event, noteId) {
        const {deleteNote} = this.props;
        deleteNote(noteId, returnHomeCallback(this.props));
    }
}

const mapStateToProps = ({noteReducers}) => ({
    notes: noteReducers.notes
});

const mapDispatchToProps = (dispatch) => ({
    deleteNote: (noteId, returnHomeCallback) => dispatch(deleteNote(noteId, returnHomeCallback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Note);
