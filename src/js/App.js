import React, {Component} from 'react';
import './App.less';
import {Route} from "react-router";
import {BrowserRouter} from "react-router-dom";
import Home from "./note/pages/Home";
import Note from "./note/pages/Note";
import NoteProducer from "./note/components/NoteProducer";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <BrowserRouter>
            <Route exact path='/' component={Home}/>
            <Route path='/notes/:id(\d+)' component={Note}/>
            <Route path='/notes/create' component={NoteProducer}/>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
