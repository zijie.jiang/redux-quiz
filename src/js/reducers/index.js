import {combineReducers} from "redux";
import noteReducers from "../note/reducers/noteReducers";

const reducers = combineReducers({
    noteReducers
});

export default reducers;
