import noteReducers from "../js/note/reducers/noteReducers";

it('should get notes reducer', function () {
    const initState = {notes: []};
    const action = {type: 'GET_NOTES', payload: {id: 1}};
    const result = noteReducers(initState, action);
    expect(result.notes.id).toBe(1);
});

it('should other reducers', function () {
    const initState = {notes: []};
    const action = {type: 'DELETE_NOTE_BY_ID'};
    const result = noteReducers(initState, action);
    expect(result.notes).toEqual([]);
});
