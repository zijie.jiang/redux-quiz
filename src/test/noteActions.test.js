import {createNote, deleteNote, getNotes} from "../js/note/actions/noteActions";

it('should get all notes', function () {
    const responseJson = '{"id": 1, "title": "title", "description": "description"}';
    fetch.mockResponse(responseJson);
    getNotes()(result => {
        expect(result.type).toBe('GET_NOTES');
        expect(result.payload).not.toBeNull();
        expect(result.payload.id).toBe(1);
    });
});

it('should delete note', function () {
    fetch.mockResponse({});
    deleteNote(1, () => {})(result => {
        expect(result.type).toBe('DELETE_NOTE_BY_ID');
    })
});

it('should create note', function () {
    fetch.mockResponse({});
    createNote('title', 'desc', () => {})(result => {
        expect(result.type).toBe('CREATE_NOTE');
    })
});
